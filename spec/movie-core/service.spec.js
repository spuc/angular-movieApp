describe('MovieCore', function() {
  
var PopularMovies ;

var $httpBackend;

beforeEach( module('movieCore') );

beforeEach( 
	inject(function(_PopularMovies_, _$httpBackend_){
		PopularMovies = {};
    PopularMovies = _PopularMovies_;
		$httpBackend =  _$httpBackend_;
}));

afterEach(function () {
  $httpBackend.verifyNoOutstandingExpectation();
  $httpBackend.verifyNoOutstandingRequest();
});

  it('should create popular movie...', function() {
  	// '{"movieId":"tt0076759","description":"Great movie!"}'
    /*var expectedData = function (data) {     
      return angular.fromJson(data).movieId === 'tt0076759';
    }
    */

    //can also pass js objects but they must match exactly
    //var expectedData = {"movieId":"tt0076759","description":"Great movie!"};

    //string value that must match exactly:
    var expectedData = '{"movieId":"tt0076759","description":"Great movie!"}';
    //regex: movieid must match specific value but description can be anything
    var expectedData = /{"movieId":"tt0076759","description":".*"}/;

    $httpBackend.expectPOST(/./, expectedData)
      .respond(201);

    var popularMovie = new PopularMovies({
      movieId: 'tt0076759',
      description: 'Great movie!'
    });

    popularMovie.$save();

    expect($httpBackend.flush).not.toThrow();  //expect no errors

    //expect(true).toBe(true);
  });

  it('should get popular movie by id', function() {
      $httpBackend.expectGET( 'popular/tt0076759')  
        .respond(200);
      PopularMovies.get({ movieId: 'tt0076759' });

      expect($httpBackend.flush).not.toThrow();  //expect no errors

   });

  it('should update popular movie', function() {
     
    $httpBackend.expectPUT( 'popular')  
        .respond(200);

     var popularMovie = new PopularMovies({
      movieId: 'tt0076759',
      description: 'Great movie!'
    });

      PopularMovies.update();

      expect($httpBackend.flush).not.toThrow();  //expect no errors

   });

    it('should authenticate requests', function() {
        // '{ "authToken": "teddybear",  "Accept": "application/json, text/plain, */*"}'
        /*var expectedHeaders = function(headers) {
          dump(angular.mock.dump(headers));
          //return true;
          return angular.fromJson(headers).authToken === 'teddybear';
        };*/

        var expectedHeaders = { "authToken": "teddybear",  "Accept": "application/json, text/plain, */*"};



        $httpBackend.expectGET( 'popular/tt0076759', expectedHeaders )  
          .respond(200);
        PopularMovies.get({ movieId: 'tt0076759' });
        $httpBackend.flush(1);

    });

     it('should authenticate ALL requests', function() {
       

        //var headerData = { "authToken": "teddybear",  "Accept": "application/json, text/plain, */*"};
        
        var headerData = function(headers){
          return headers.authToken === 'teddybear';
        }

        var matchAny = /.*/;

        $httpBackend.whenGET( matchAny, headerData )  
          .respond(200);
          //expect used for precision
        $httpBackend.expectPOST( matchAny, matchAny, headerData )  
          .respond(200);

           $httpBackend.expectPUT( matchAny, matchAny, headerData )  
          .respond(200);
 
          $httpBackend.expectDELETE( matchAny, headerData )  
          .respond(200);

          var popularMovie = { id: 'tt0076759', description: 'This movie is great!' };

          PopularMovies.query();
          PopularMovies.get( { id: 'tt0076759'});
          new PopularMovies(popularMovie).$save();
          new PopularMovies(popularMovie).$update();
          new PopularMovies(popularMovie).$remove();


          /*
          $httpBackend.flush(1);
          $httpBackend.flush(1);
          $httpBackend.flush(1);
          $httpBackend.flush(1);
          $httpBackend.flush(1);
          */
          expect($httpBackend.flush).not.toThrow();

    });

  
});

