describe('Search Controller', function() {
  
var x ;

var $httpBackend;

var $scope;
var $location = {};
var $timeout;

beforeEach( module('movieApp') );

beforeEach( inject(function(_$controller_, _$location_, _$timeout_){
    $scope = {};
    $location = _$location_;
    $timeout = _$timeout_;  //locals object passed to the controller service
   
   
    //adding and injecting to locals object...
    _$controller_('SearchController', { 
      $scope: $scope, 
      $location: $location,
      $timeout: $timeout
    });
     
  }));  


  it('should redirect to the query results page for non-empty query..', function() {
    $scope.query = 'star wars';
    $scope.search();
    expect($location.url()).toBe('/results?q=star%20wars');

  });

 
 /* it('should not redirect to query results for empty query', function() {
      $scope.query = '';
      $scope.search();
      expect($location.url()).toBe('');
   });*/

    it('should redirect after 1 second of keyboard inactivity', function() {
      $scope.query = 'star wars';
      $scope.keyup();
      //$timeout.flush();  //cause the timeout to happen - default without parememters runs all timeouts
      $timeout.flush();
      //$timeout.verifyNoPendingTasks();  //useful in verifying mistakes -> provides a more useful error message in console
      expect($timeout.verifyNoPendingTasks).not.toThrow(); //verify there are no pending runout functions
      expect($location.url()).toBe('/results?q=star%20wars');
     
   });

    it('should cancel timeout in keydown', function() {
      $scope.query = 'star wars';
      $scope.keyup();
      $scope.keydown();
      expect($timeout.verifyNoPendingTasks).not.toThrow();
     
   });

    it('should cancel timeout in search', function() {
      $scope.query = 'star wars';
      $scope.keyup();
      $scope.search();
      expect($timeout.verifyNoPendingTasks).not.toThrow();
     
   });


});

