angular.module('movieApp')
	.controller('ResultsController', function($scope, $location, $exceptionHandler, $log, omdbApi){
		/*$scope.results = [];
		$scope.results.push({ data: { Title: 'Star Wars: Episode IV - a new hope' }});
		$scope.results.push({ data: { Title: 'Star Wars: Episode V - the empire strikes back' }});
		$scope.results.push({ data: { Title: 'Star Wars: Episode VI - Return of the Jedi' }});
		*/
		var query = $location.search().q;

		$log.debug('Controller loaded with query: ', query);
		
		omdbApi.search(query)
	  		.then(function(data){
	  			$log.debug('Data returned for query: ', query, data);
	  			$scope.results = data.Search;
	  			
	  		})
	  		.catch(function (e){
	  			//$scope.errorMessage = 'Something went wrong!';
	  			//throw 'Something went wrong!';
	  			$exceptionHandler(e);
	  			//$exceptionHandler(e);
	  			//$exceptionHandler('something else ..');
	  		});

  		$scope.expand = function expand(index,id) {
  			omdbApi.find(id)
  				.then(function(data) {
  					$scope.results[index].data = data;
  					$scope.results[index].open = true;
  				});
  		};

});